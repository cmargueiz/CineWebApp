/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.controller;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.ParameterExpression;

/**
 *
 * @author cmargueiz
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

public List<T> findRange(int inicio, int maximo) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(maximo);
        q.setFirstResult(inicio);
        return q.getResultList();
    }

    public Integer count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public List<T> findByCostumer(String atributo, String valor) {
        javax.persistence.criteria.CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery<T> criteriaQuery = builder.createQuery(entityClass);
        javax.persistence.criteria.Root<T> c = criteriaQuery.from(entityClass);
        ParameterExpression<String> parameter = parameter = builder.parameter(String.class);
        criteriaQuery.select(c).where(builder.like(c.get(atributo), parameter));
        javax.persistence.TypedQuery<T> query = getEntityManager().createQuery(criteriaQuery);
        query.setParameter(parameter, "%" + valor + "%");
        return query.getResultList();

    }
    
}
