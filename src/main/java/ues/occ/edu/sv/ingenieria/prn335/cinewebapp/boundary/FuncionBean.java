/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.controller.SucursalFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.controller.FuncionFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.primefaces.model.LazyDataModel;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.AsientoSala;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Funcion;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Sala;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Sucursal;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.utilities.EstadoCRUD;

/**
 *
 * @author cmargueiz
 */
@Named
@ViewScoped
public class FuncionBean extends AbstractFrmBean<Funcion> implements Serializable {
    
    final static String RESOURCE = "/funcion";
    private Integer pestanaActiva;
    private Integer idSucursal;
    
    private Integer idSala;
    private List<Sucursal> ListaDeSucursales;
    private List<Sala> ListaDeSalas;
    private List<Funcion> ListaDeSFunciones;
    private List<AsientoSala> ListaDeAsientos;
    private Calendar fechaActual = Calendar.getInstance();
    private List<AsientoSala> ListaAsientoSeleccionado;
    
    @Inject
    FuncionFacade funcionFacade;
    
    @Inject
    SucursalFacade sucursalFacade;
    
    @PostConstruct
    @Override
    public void inicializar() {
        super.client = ClientBuilder.newClient();
        
        if (listaSucursal() != null && getListaFunciones() != null) {
            ListaDeSucursales = listaSucursal();
            ListaDeSFunciones = new ArrayList<>();
            for (Funcion f : getListaFunciones()) {
                if (f.getFecha().after(fechaActual.getTime()) || f.getFecha().compareTo(fechaActual.getTime()) == 0) {
                    ListaDeSFunciones.add(f);
                }
            }
            
        } else {
            ListaDeSFunciones = Collections.EMPTY_LIST;
        }
        this.pestanaActiva = 0;
        setEstado(EstadoCRUD.NONE);
    }
    
    @Override
    public Funcion getEntityById(String rowKey) {
        if (rowKey != null && !rowKey.isEmpty()) {
            try {
                Integer search = new Integer(rowKey);
                for (Funcion object : this.List) {
                    if (object.getIdFuncion().compareTo(search) == 0) {
                        return object;
                    }
                }
            } catch (NumberFormatException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return null;
    }
    
    @Override
    public Funcion getRegistro() {
        if (registro == null) {
            registro = new Funcion();
        }
        return super.getRegistro();
    }
    
    @Override
    public void setRegistro(Funcion registro) {
        super.setRegistro(registro); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Object getEntity(Funcion object) {
        if (object != null) {
            return object.getIdFuncion();
        }
        return null;
    }
    
    @Override
    public LazyDataModel<Funcion> getModelo() {
        return super.getModelo(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String getUrlResurce() {
        return RESOURCE;
    }
    
    @Override
    public Integer getIdEntity() {
        return registro.getIdFuncion();
    }
    
    @Override
    public void setEstado(EstadoCRUD estado) {
        super.setEstado(estado); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public EstadoCRUD getEstado() {
        return super.getEstado(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Integer getIdSucursal() {
        return idSucursal;
    }
    
    public void setIdSucursal(Integer idSucursal) {
        this.idSucursal = idSucursal;
    }
    
    public Integer getIdSala() {
        return idSala;
    }
    
    public void setIdSala(Integer idSala) {
        this.idSala = idSala;
    }
    
    public List<Sucursal> getListaDeSucursales() {
        return ListaDeSucursales;
    }
    
    public void setListaDeSucursales(List<Sucursal> ListaDeSucursales) {
        this.ListaDeSucursales = ListaDeSucursales;
    }
    
    public List<Funcion> getListaDeSFunciones() {
        return ListaDeSFunciones;
    }
    
    public void setListaDeSFunciones(List<Funcion> ListaDeSFunciones) {
        this.ListaDeSFunciones = ListaDeSFunciones;
    }
    
    public List<Sala> getListaDeSalas() {
        return ListaDeSalas;
    }
    
    public void setListaDeSalas(List<Sala> ListaDeSalas) {
        this.ListaDeSalas = ListaDeSalas;
    }
    
    public List<AsientoSala> getListaDeAsientos() {
        return ListaDeAsientos;
    }
    
    public void setListaDeAsientos(List<AsientoSala> ListaDeAsientos) {
        this.ListaDeAsientos = ListaDeAsientos;
    }
    
    public List<AsientoSala> getListaAsientoSeleccionado() {
        return ListaAsientoSeleccionado;
    }
    
    public void setListaAsientoSeleccionado(List<AsientoSala> ListaAsientoSeleccionado) {
        this.ListaAsientoSeleccionado = ListaAsientoSeleccionado;
    }
    
    public Integer getPestanaActiva() {
        return pestanaActiva;
    }
    
    public void setPestanaActiva(Integer pestanaActiva) {
        this.pestanaActiva = pestanaActiva;
    }

    //Void Eventos
    public void btnSeleccionarSucursal(ActionEvent ae) {
        if (idSucursal != null) {
            ListaDeSalas = listaSalaBySucursal(idSucursal);
            inicializar();
            setEstado(EstadoCRUD.SUCURSALSELECCIONADA);
            this.pestanaActiva = 1;
        }
    }
    
    public void idSucursalSeleccionado(ValueChangeEvent ce) {
        
        if (ce != null) {
            
            ListaDeSalas = listaSalaBySucursal(idSucursal);
            
        }
        
    }
    
    public void btnSeleccionarSala(ActionEvent ae) {
        if (idSala != null) {
            System.out.println(idSala);
            ListaDeAsientos = listaAsientoSalaBySala(idSala);
            setEstado(EstadoCRUD.SALASELECCIONADA);
            this.pestanaActiva = 2;
        }
    }
    
    public void btnSeleccionarAsientos(ActionEvent ae) {
        if (idSala != null) {
            System.out.println(idSala);
            this.ListaDeSalas = this.listaSalaBySucursal(idSucursal);
            setEstado(EstadoCRUD.ASIENTOSELECCIONADO);
        }
    }
    
    public void btnCancelar(ActionEvent ae){
        
            
            idSala=null;
            idSucursal=null;
            inicializar();
            setEstado(EstadoCRUD.NONE);
        
    }
    public void btnConfirmarReserva(ValueChangeEvent ae) {
        
        if (ae != null) {
            List<AsientoSala> asientosReservados = (List<AsientoSala>) ae.getNewValue();
            
            for (AsientoSala a : asientosReservados) {
                
            }
        }
    }
    
    public void crearReserva(AsientoSala asiento, Integer idSala, Integer idDescuento, Double precioBase) {
        
    }
    
    public List<Funcion> getListaFunciones() {
        if (funcionFacade != null) {
            return funcionFacade.findAll();
        }
        return Collections.EMPTY_LIST;
    }
    
    public List<Sucursal> listaSucursal() {
        if (sucursalFacade.findAll()!=null) {
           return sucursalFacade.findAll();
        }
        return Collections.EMPTY_LIST;
    }
    
    public List<Sala> listaSalaBySucursal(Integer idSucursal) {
        if (funcionFacade != null) {
            return funcionFacade.findSalaBySucursal(idSucursal);
        }
        return Collections.EMPTY_LIST;
    }
    
    public List<AsientoSala> listaAsientoSalaBySala(Integer idSala) {
        if (funcionFacade != null) {
            return funcionFacade.findAsientoBySala(idSala);
        }
        return Collections.EMPTY_LIST;
    }
}
