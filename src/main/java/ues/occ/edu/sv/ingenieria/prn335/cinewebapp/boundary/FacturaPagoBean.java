/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Factura;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Pago;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.TipoDescuento;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.TipoPago;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.utilities.EstadoCRUD;

/**
 *
 * @author marcela
 */
@Named
@ViewScoped
public class FacturaPagoBean extends AbstractFrmBean<Factura> implements Serializable {

    final static String RESOURCE = "/factura";

    private Integer idFactura;
    private Integer idPago;
    private List<Pago> listaDePago;
    private List<Factura> listaDeFactura;
    private List<TipoPago> listaDeTipoPago;
    private List<TipoDescuento> listaDeTipoDescuento;

    @PostConstruct
    @Override
    public void inicializar() {
        super.inicializar();

    }

    @Override
    public void setEstado(EstadoCRUD estado) {
        super.setEstado(estado); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EstadoCRUD getEstado() {
        return super.getEstado(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setRegistro(Factura registro) {
        super.setRegistro(registro); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Factura getRegistro() {
        return super.getRegistro(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getEntity(Factura object) {
        if (object != null) {
            return object.getIdFactura();
        }
        return null;
    }

    @Override
    public Factura getEntityById(String rowKey) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getUrlResurce() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getIdEntity() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdPago() {
        return idPago;
    }

    public void setIdPago(Integer idPago) {
        this.idPago = idPago;
    }

    public List<Pago> getListaDePago() {
        return listaDePago;
    }

    public void setListaDePago(List<Pago> listaDePago) {
        this.listaDePago = listaDePago;
    }

    public List<Factura> getListaDeFactura() {
        return listaDeFactura;
    }

    public void setListaDeFactura(List<Factura> listaDeFactura) {
        this.listaDeFactura = listaDeFactura;
    }

    public List<Pago> listaPago() {
        if (super.client != null) {
            try {
                WebTarget target = super.client.target("http://localhost:8080/CineWebApp/resources/pago");
                return target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Pago>>() {
                });
            } catch (NullPointerException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return Collections.EMPTY_LIST;
    }

    public List<Factura> listaFactura() {
        if (super.client != null) {
            try {
                WebTarget target = super.client.target("http://localhost:8080/CineWebApp/resources/factura");
                return target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Factura>>() {
                });
            } catch (NullPointerException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return Collections.EMPTY_LIST;
    }

    public List<TipoPago> listaTipoPagos() {
        if (super.client != null) {
            try {
                WebTarget target = super.client.target("http://localhost:8080/CineWebApp/resources/tipopago");
                return target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<TipoPago>>() {
                });
            } catch (NullPointerException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return Collections.EMPTY_LIST;
    }

    public List<TipoDescuento> listaTipoDescuentos() {
        if (super.client != null) {
            try {
                WebTarget target = super.client.target("http://localhost:8080/CineWebApp/resources/tipodescuento");
                return target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<TipoDescuento>>() {
                });
            } catch (NullPointerException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return Collections.EMPTY_LIST;
    }

    public Factura create(Factura registro) {
        if (registro != null && registro.getIdFactura() != null) {
            try {
                if (super.client != null) {
                    WebTarget target = super.client.target("http://localhost:8080/CineWebApp/resources/factura/create");
                    Factura salida = target.request(MediaType.APPLICATION_JSON).post(Entity.entity(registro, MediaType.APPLICATION_JSON_TYPE), Factura.class);
                    if(salida!=null && salida.getIdFactura()!=null){
                        return salida;
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return null;
    }
}
